import ButtonMui, { ButtonProps as ButtonPropsMui } from '@mui/material/Button';

interface ButtonProps extends ButtonPropsMui { }

const Button = (props: ButtonProps) => (
    <ButtonMui {...props} />
)

export default Button;
export type { ButtonProps };