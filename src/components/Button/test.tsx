import { describe, expect, it } from 'vitest'
import { render, screen } from '../../utils/test-utils'
import Button from '.'

describe('<Button />', async () => {
    it('should render buttom component ', () => {
        render(<Button> NAME_BUTTON </Button>)
        expect(screen.getByRole('button', { name: 'NAME_BUTTON' })).toBeInTheDocument()
    })

})
