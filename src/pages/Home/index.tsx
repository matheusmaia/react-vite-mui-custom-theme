import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';

const Home = () => {
    return (
        <div>
            <h1> Home </h1>
            <h1> Home2 </h1>
            <Button variant="contained">Mui Button</Button>
            <TextField id="outlined-basic" label="Outlined" variant="outlined" />
            <TextField id="filled-basic" label="Filled" variant="filled" />
            <TextField id="standard-basic" label="Standard" variant="standard" />

            <ListItem role={'listitem'} aria-label='Mui List'>
                <ListItemText primary="Mui List" />
            </ListItem>
        </div>
    );
};

export default Home;