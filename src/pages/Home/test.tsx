import { describe, expect, it } from 'vitest'
import Home from '.'
import { render, screen } from '../../utils/test-utils'

describe('Simple working test', async () => {
    it('the title is visible', () => {
        render(<Home />)
        expect(screen.getByText("Home")).toBeInTheDocument()
    })

    it('should be a buttton into de home page', () => {
        render(<Home />)
        expect(screen.getByRole('button', { name:'Mui Button'})).toBeInTheDocument()
    })

    it('should be three inputs into de home page', () => {
        render(<Home />)
        expect(screen.getByRole('textbox', { name:'Outlined'})).toBeInTheDocument()
        expect(screen.getByRole('textbox', { name:'Filled'})).toBeInTheDocument()
        expect(screen.getByRole('textbox', { name:'Standard'})).toBeInTheDocument()
    })

    it('should be a list item into de home page', () => {
        render(<Home />)
        expect(screen.getByRole('listitem', { name:'Mui List'})).toBeInTheDocument()
    })
})
