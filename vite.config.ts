/// <reference types="vitest" />
/// <reference types="vite/client" />

import react from '@vitejs/plugin-react'
import { defineConfig } from 'vite'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  test: {
    globals: true,
    include: ['**/test.tsx'],
    environment: 'jsdom',
    setupFiles: './src/test/setup.ts',
    coverage: {
      enabled: true,
      excludeNodeModules: true,
      reporter: ['text', 'html', 'cobertura', 'json'],
      all: true,
      exclude: [
          '**vite**',
          'src/**.d.ts',
          'node_modules/**',
          'src/utilities/**',
          '.storybook/**',
          'storybook-static/**',
          '_helpers/**',
          'dist/**',
          'public/**',
          'styleDictionary/**',
      ],
    },
  },
  base: '/react-vite-mui-custom-theme/'
})